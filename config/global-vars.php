<?php
/**
 * Created by PhpStorm.
 * User: malina
 * Date: 17.12.2017
 * Time: 18:40
 */

/* Инфо сообщения */
const PARSING_START = "\r\n{date} начинаем парсинг с proxy {proxy}.";
const LIVE_FOOTBALL_PAGE_OK = 'Есть данные на странице футбола.';
const CHAMPIONSHIPS_OK = 'Есть чемпионаты.';
const END_PARSING_INFO = '{date} матчей - {online}, прокси - {proxy}, время - {time} c.';
const START_GET_BALANCE_INFO = '{date} получаем баланс, прокси - {proxy}.';
const END_GET_BALANCE_INFO = '{date} прокси - {proxy}, баланс - {balance}, время - {time} c.';
const PROCESS_CHAMPIONSHIP = 'Найден чемпионат {name}.';
const CHAMPIONSHIP_MATCHES_FIND = 'В нем {count} матчей.';
const MATCH_SAVED_TO_DB = 'Матч {names} сохранен в базу.';
const SKIP_BET_BAD_CHAMPIONSHIP = 'Ставка на матч {names} не сделана - опасный чемпионат {id}';
const MATCH_SAVED_FINISHED = 'Матч {names} закончился с профитом {profit}.';
const MATCH_SAVED_FINISHED_PREVIOUSLY = 'Матч {names} закончился предварительно с профитом -100.';
const MATCH_ALREADY_SAVED_TO_DB = 'Матч {names} уже сохранен в базу.';
const PROXY_PARSING_TIME = 'Прокси {proxy}, время парсинга {url}  -  {time} c.';
const GOOD_CURRENT_COOKIES = 'Текущие cookies актуальны.';
const COOKIES_START_REFRESH = 'Начинаю обновление cookies.';
const BALANCE_NOT_FORCE = 'Не форсированный режим получения баланса.';
const BET_START = '{date} Начата серия ставок после матча {id}.';
const BET_END = '{date} Закончена серия ставок с доходом {profit}.';
const SET_ONE_CLICK_BET_SUCCESS = 'Успешно установлена ставка по клику в размере {bet}.';
const SUCCESS_PLACE_BET = '{date} Сделана ставка в матче {name} ({score}) с коэф-том {rate} с {loops} попытки - время {time} с.';
const SEND_EMAIL = '{date} Отправили письмо с темой: {subj} и текстом: {text} с {loops} попытки.';

/* Темы писем */
const BALANCE_SUBJECT = 'Баланс/авторизация событие';
const BET_SUBJECT = 'Ставки/стратегии событие';


/* Каналы */
const PARSING_CHANNEL = 'parsing';
const PROXY_CHANNEL = 'proxy';
const BALANCE_CHANNEL = 'balance';
const BET_CHANNEL = 'bets';
const EMAIL_CHANNEL = 'email';

/* Ошибки */
const EMPTY_FOOTBALL_PAGE = 'Пустая страница футбола.';
const EMPTY_RATES_PAGE = 'Пустая страница коэффициентов матча {id}.';
const IP_IS_BLOCKED = 'IP {ip} заблокирован.';
const MATCHES_NOT_FOUND = 'Не найдено матчей онлайн!';
const JSON_PARSE_SELECTOR_FOOTBALL_PAGE_ERROR = 'Селектор liveEventsContent не найден в JSON объекте.';
const GET_CHAMPIONSHIPS_ERROR = 'Ошибка разбивки на чемпионаты.';
const EMPTY_CHAMPIONSHIP_NAME = 'Пустое название чемпионата.';
const NO_MATCHES_IN_CHAMPIONSHIP = 'Нет матчей в чемпионате {name}.';
const WRONG_MATCH_ID = 'Пустой id матча на сайте в чемпионате {name}.';
const WRONG_COMMAND_NAMES = 'Ошибка получения названий команд матча {id}.';
const MINUTES_PARSE_ERROR = 'Ошибка парсинга минут матча {id}.';
const GLOBAL_SCORE_PARSE_ERROR = 'Ошибка парсинга глобального счета матча {id}.';
const GLOBAL_SCORE_ARRAY_PARSE_ERROR = 'Ошибка получения массива глобального счета матча {id}.';
const SCORE_BLOCK_PARSE_ERROR = 'Ошибка парсинга счета матча (блока) {id}.';
const SCORE_PARSE_ERROR = 'Ошибка парсинга счета матча {id}.';
const TIME_BLOCK_PARSE_ERROR = 'Ошибка парсинга времени матча (блока) {id}.';
const TIME_PARSE_ERROR = 'Ошибка парсинга времени матча {id}.';
const CANT_LOCK_FILE_TO_WRITE = 'Не могу заблокировать файл {name} для записи!';
const CANT_LOCK_FILE_TO_READ = 'Не могу заблокировать файл {name} для чтения!';
const NO_PROXIES = 'Нет прокси!';
const EMPTY_PROXY = 'Пустой прокси!';
const NO_MATCHES = 'Нет матчей!';
const BALANCE_NOT_FOUND = 'Не найден баланс в кабинете!';
const COOKIES_CANT_REFRESH = 'Не могу обновить cookies, статус: {status}';
const COOKIES_NEED_REFRESH = 'Требуется обновить cookies!';
const CANT_SET_ONE_CLICK_BET = 'Не могу установить ставку по клику!';
const CANT_FIND_RESERVED_RATE = '{date} Не могу найти строку Total_Goals_-_1st_Half в матче {id}!';
const CANT_FIND_EVENT_NAME = '{date} Не могу найти строку с именами команд data-event-name в матче {id}!';
const CANT_FIND_DATA_SEL = '{date} Не могу найти строку data-sel=\'({"sn":"Меньше в матче {id}!';
const CANT_CREATE_TICKET = '{date} Тикет ставки не создан POST DATA: {data} в матче {id}!';
const WRONG_PLACE_BET = '{date} Ставка не сделана с {loops} попытки в матче {id}!';
const SEND_EMAIL_ERROR = '{date} Ошибка тправки письма с темой: {subj} и текстом: {text}. Ошибки: {errors}';