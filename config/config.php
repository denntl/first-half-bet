<?php
/**
 * Created by PhpStorm.
 * User: malina
 * Date: 17.08.2017
 * Time: 23:31
 */

return [
    'mysql' => [
        'db'       => 'first-half-bet',
        'host'     => 'localhost',
        'password' =>  555333,
        'user'     => 'root'
    ],
    'redis' => [
        'host' => 'localhost',
        'port' => 6379
    ],
    'bet' => 7.1,
    'send-mail' => false
];