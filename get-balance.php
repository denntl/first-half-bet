<?php
/**
 * Created by PhpStorm.
 * User: malina
 * Date: 29.12.2017
 * Time: 23:46
 */

if($_GET['run']) {

    require('./config/global-vars.php');

    require('./google-api-php-client-2.2.1/vendor/autoload.php');
    require('./classes/Gmail.php');
    require('./classes/Time.php');
    require('./classes/Db.php');
    require('./classes/Log.php');
    require('./classes/Curl.php');
    require('./classes/Marathon.php');
    require('./classes/Proxy.php');
    require('./classes/Strategy.php');
    require('./classes/Service.php');

    function getBalance($service, $accountUrl, $loginUrl, $oneClickUrl)
    {

        $start = microtime(true);

        $isLogin = [];

        $service->log->balance(
            START_GET_BALANCE_INFO,
            'cyan',
            ['{date}', '{proxy}'],
            [$service->time->getDate(), $service->proxy->currentProxy]
        );

        if (!$_GET['force-login']) {

            $service->log->balance(BALANCE_NOT_FORCE, 'greenBright');

            $accountPage = $service->curl->getUrl($accountUrl,
                $service->proxy->currentProxyData, '', true, 5);

            preg_match('#Ваш номер счёта#', $accountPage, $isLogin);
            preg_match('#data-punter-balance-value="(\d+.\d+)">#', $accountPage,
                $balance);

            if ($isLogin[0]) {
                $service->log->balance(GOOD_CURRENT_COOKIES, 'greenBright');
            } else {
                $service->log->balance(COOKIES_NEED_REFRESH, 'yellowBright');
            }
        }

        if ($_GET['force-login'] || !$isLogin[0]) {

            $service->log->balance(COOKIES_START_REFRESH, 'yellowBright');

            $loginResult = $service->curl->getUrl($loginUrl,
                $service->proxy->currentProxyData,
                'login=denntl&login_password=sOU9lsuIq6fFVHI', true, 5);

            $loginResult = json_decode($loginResult, true);

            if ($loginResult['loginResult'] != "SUCCESS") {
                $service->log->balance(
                    COOKIES_CANT_REFRESH,
                    'redBright',
                    '{status}',
                    $loginResult['loginResult']
                );
                return false;
            }

            $accountPage = $service->curl->getUrl($accountUrl,
                $service->proxy->currentProxyData, '', true, 5);

            preg_match('#Ваш номер счёта#', $accountPage, $isLogin);
            preg_match('#data-punter-balance-value="(\d+.\d+)">#', $accountPage,
                $balance);

            if ($isLogin[0]) {
                $service->log->balance(GOOD_CURRENT_COOKIES, 'greenBright');
            } else {
                $service->log->balance(COOKIES_NEED_REFRESH, 'redBright');
            }
        }

        if (!isset($balance[1])) {
            $service->log->balance(BALANCE_NOT_FOUND, 'redBright');
            return false;
        }

        $balance = (float)$balance[1];

        $oneClickResult = $service->curl->getUrl($oneClickUrl,
            $service->proxy->currentProxyData,
            "chd=true&st=" . $service->config['bet'], true, 5);
        $oneClickResult = json_decode($oneClickResult, true);

        if (!$oneClickResult['isLogged']) {
            $service->log->balance(CANT_SET_ONE_CLICK_BET, 'redBright');
            return false;
        }

        $service->log->balance(SET_ONE_CLICK_BET_SUCCESS, 'green', '{bet}',
            $service->config['bet']);

        $time = round(microtime(true) - $start, 2);

        $service->log->balance(
            END_GET_BALANCE_INFO,
            'cyan',
            ['{date}', '{balance}', '{proxy}', '{time}'],
            [
                $service->time->getDate(),
                $balance,
                $service->proxy->currentProxy,
                $time
            ]
        );

        $service->log->setLoginTime();

        return true;
    }

    $service = Service::getInstance();

    $accountUrl = 'https://www.marathonbet.com/su/myaccount/myaccount.htm';
    $loginUrl = 'https://www.marathonbet.com:443/su/login.htm';
    $oneClickUrl
        = 'https://www.marathonbet.com/su/betslip/updatebetinoneclick.htm';

    for ($i = 0; $i < 5; $i++) {
        if (getBalance($service, $accountUrl, $loginUrl, $oneClickUrl)) {
            break;
        }
        $service->proxy->refreshProxyNumber();
    }
}