<?php
/**
 * Created by PhpStorm.
 * User: malina
 * Date: 25.12.2017
 * Time: 20:55
 */

$start = microtime(true);

require('./config/global-vars.php');

require('./google-api-php-client-2.2.1/vendor/autoload.php');
require('./classes/Gmail.php');
require('./classes/Time.php');
require('./classes/Db.php');
require('./classes/Log.php');
require('./classes/Curl.php');
require('./classes/Marathon.php');
require('./classes/Proxy.php');
require('./classes/Strategy.php');
require('./classes/Service.php');

$service = Service::getInstance();

if($_GET['send-start-email']){
    $service->log->email('','Запущен парсер ' . $service->time->getDate(), 'green');
}

$marathon = new Marathon();

$time = round(microtime(true) - $start, 2);

$endMessage = str_replace(
    ['{date}','{online}', '{proxy}', '{time}'],
    [$service->time->getDate(), $marathon->onlineCount, $service->proxy->currentProxy, $time],
    END_PARSING_INFO
);

$service->db->emit($endMessage,PARSING_CHANNEL,'cyan');

echo $endMessage;