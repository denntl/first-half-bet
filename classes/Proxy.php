<?php

/**
 * Created by PhpStorm.
 * User: malina
 * Date: 19.12.2017
 * Time: 13:27
 */
class Proxy {

    private $_db;
    private $_log;
    private $_time;

    protected function __clone() {}

    private static $_instance = null;
    private static $_proxyPath = __DIR__ . '/../config/proxies.txt';
    private static $_currentProxyPath = __DIR__ . '/../config/currentProxy.txt';
    private static $_currentProxyNumber = 0;
    private static $_proxiesCount = 0;
    private static $_proxies = [];

    public $currentProxy = '';
    public $currentProxyData = [];

    static public function getInstance() {
        if(is_null(self::$_instance)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct(){

        $this->_db = Db::getInstance();
        $this->_log = Log::getInstance();
        $this->_time = Time::getInstance();

        $this->refreshProxyNumber();
    }

    public function refreshProxyNumber(){

        $this->getProxies();

        if(self::$_proxiesCount == 0) {

            echo NO_PROXIES  . '   ';
            $this->_log->error(NO_PROXIES);

        } else {

            if(file_exists(self::$_currentProxyPath)){
                self::$_currentProxyNumber = (int) @file_get_contents(self::$_currentProxyPath) + 1;
                if(self::$_currentProxyNumber == self::$_proxiesCount){
                    self::$_currentProxyNumber = 0;
                }
            }

            $ft = fopen(self::$_currentProxyPath, 'w');
            fwrite($ft, self::$_currentProxyNumber);
            fclose($ft);

            $this->currentProxyData = explode("-", self::$_proxies[self::$_currentProxyNumber]);
            $this->currentProxy = $this->currentProxyData[0];

            if(empty($this->currentProxy)){
                echo EMPTY_PROXY . '   ';
                $this->_log->error(EMPTY_PROXY);
            }
        }
    }

    private function getProxies(){
        self::$_proxies = file(self::$_proxyPath, FILE_IGNORE_NEW_LINES);
        self::$_proxiesCount = count(self::$_proxies);
    }

    function logParsingTime($startTime, $url, $currentProxy){

        $parseTime = round(microtime(true) - $startTime, 2);

        $url = str_replace('https://www.marathonbet.com/su/live', '', $url);

        $messageText = str_replace(['{proxy}', '{url}', '{time}'], [$currentProxy, $url, $parseTime], PROXY_PARSING_TIME);

        if($parseTime > 19.99 || $parseTime < 0.2){
            $this->_log->proxyError($messageText);
        } else {
            $this->_db->emit($messageText,PROXY_CHANNEL, 'white');
        }
    }
}