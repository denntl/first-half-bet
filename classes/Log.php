<?php

/**
 * Created by PhpStorm.
 * User: malina
 * Date: 05.08.2017
 * Time: 11:29
 */
class Log {

    private $_currentLogsPath = __DIR__ . '/../logs/{date}';
    private $_logsDirPath = __DIR__ . '/../logs';

    private $_infoPath = __DIR__ . '/../logs/{date}/info.txt';
    private $_errorPath = __DIR__ . '/../logs/{date}/errors.txt';
    private $_customPath = __DIR__ . '/../logs/{date}/custom.txt';
    private $_proxyPath = __DIR__ . '/../logs/{date}/proxy-errors.txt';
    private $_betsPath = __DIR__ . '/../logs/{date}/bets.txt';
    private $_balancePath = __DIR__ . '/../logs/{date}/balance.txt';
    private $_emailPath = __DIR__ . '/../logs/{date}/email.txt';

    private $_firstLoseMatchPath = __DIR__ . '/../config/first-lose-match-id.txt';
    static private $_loginTimePath = __DIR__ . '/../config/login-time.txt';
    static private $_gmailTokenPath = __DIR__ . '/../config/gmail-token.txt';
    static private $_gmailTokenExamplePath = __DIR__ . '/../config/gmail-token-example.txt';
    static private $_badChampionshipsPath = __DIR__ . '/../config/bad-championships.txt';

    protected function __clone() {}

    static private $_instance = null;
    private $_dbInstance = null;
    private $_config = null;
    private $_timeInstance = null;
    private $_gmailInstance = null;

    static private $_badChampionships = [];

    static public function getInstance() {
        if(is_null(self::$_instance)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct(){

        $this->_config = (array) require_once(__DIR__ . '/../config/config.php');

        $this->_dbInstance = Db::getInstance();
        $this->_timeInstance = Time::getInstance();
        $this->_gmailInstance = Gmail::getInstance();

        $path = $this->setDateToPath($this->_currentLogsPath);
        if(! is_dir($path)){
            if(! mkdir($path, 0777, true)){
                echo "Беда с созданием папки логов с датой $path";
            }
        }

        if(! is_dir($this->_logsDirPath)){
            if(! mkdir($this->_logsDirPath, 0777, true)){
                echo "Беда с созданием папки логов " . $this->_logsDirPath;
            }
        }
    }

    static private function getLastItem($filePath){
        $lastLog = '';
        if(file_exists($filePath)){
            $logs = file($filePath, FILE_IGNORE_NEW_LINES);
            $lastLog = $logs[count($logs) - 1];
        }
        return $lastLog;
    }

    function setFirstLoseMatch($matchId){
        self::addToFile($this->_firstLoseMatchPath, $matchId, true);
    }

    function deleteFirstLoseMatch(){
        unlink($this->_firstLoseMatchPath);
    }

    function getFirstLoseMatch(){
        return (int) @file_get_contents($this->_firstLoseMatchPath);
    }

    function getLoginTime(){
        return @file_get_contents(self::$_loginTimePath);
    }

    static function setBadChampionships($badChampionships){
        self::$_badChampionships = $badChampionships;
        self::addToFile(self::$_badChampionshipsPath, $badChampionships, true, 'w');
    }

    static function getBadChampionships(){
        if(empty(self::$_badChampionships)){
            self::$_badChampionships = 11111111111111; //file_get_contents(self::$_badChampionshipsPath);
        }
        return self::$_badChampionships;
    }

    function setLoginTime(){
        self::addToFile(self::$_loginTimePath, time(), true, 'w');
    }

    static function setGmailToken($token){
        self::addToFile(self::$_gmailTokenPath, $token, true, 'w');
    }

    static function copyGmailToken(){
        return copy(self::$_gmailTokenExamplePath, self::$_gmailTokenPath);
    }

    static function getGmailToken(){
        return @file_get_contents(self::$_gmailTokenPath);
    }

    static function getGmailTokenExample(){
        return @file_get_contents(self::$_gmailTokenExamplePath);
    }

    function info($text, $search = '', $replace = ''){
        if($search){
            $text = str_replace($search, $replace, $text);
        }
        self::addToFile($this->setDateToPath($this->_infoPath), $text);
    }

    function balance($text, $color = '', $search = '', $replace = ''){
        if($search){
            $text = str_replace($search, $replace, $text);
        }
        if($color){
            $this->_dbInstance->emit(
                $text,
                BALANCE_CHANNEL,
                $color
            );
            $this->email($text, BALANCE_SUBJECT, $color);
        }

        self::addToFile($this->setDateToPath($this->_balancePath), $text . PHP_EOL, true);
    }

    function email($text, $subject, $color = '', $search = '', $replace = ''){

        if(! $this->_config['send-mail']){
            return;
        }

        if($search){
            $text = str_replace($search, $replace, $text);
        }

        $style = $color ? 'width: 300px; height: 100px; margin-bottom: 30px; filter: brightness(150%); background-color: '. str_replace('Bright', '', $color) : '';

        $emailSend = $this->_gmailInstance->sendMail($subject, $text, $style);
        if($emailSend['status'] == 'ok'){
            $msg = str_replace(
                [
                    '{date}',
                    '{subj}',
                    '{text}',
                    '{loops}'
                ],
                [$this->_timeInstance->getDate(), $subject, $text, $emailSend['loops']],
                SEND_EMAIL
            );
        } else {
            $msg = str_replace(
                [
                    '{date}',
                    '{subj}',
                    '{text}',
                    '{errors}'
                ],
                [$this->_timeInstance->getDate(), $subject, $text, $emailSend['errors']],
                SEND_EMAIL_ERROR
            );
        }

        $this->_dbInstance->emit(
            $msg,
            EMAIL_CHANNEL,
            $emailSend['status'] == 'ok' ? 'whiteBright' : 'redBright'
        );

        self::addToFile($this->setDateToPath($this->_emailPath), $msg . PHP_EOL, true);
    }

    function bet($text, $color = '', $search = '', $replace = ''){
        if($search){
            $text = str_replace($search, $replace, $text);
        }
        if($color){
            $this->_dbInstance->emit(
                $text,
                BET_CHANNEL,
                $color
            );
            $this->email($text, BET_SUBJECT, $color);
        }
        self::addToFile($this->setDateToPath($this->_betsPath), $text . PHP_EOL, true);
    }

    function error($text, $search = '', $replace = ''){
        if($search){
            $text = str_replace($search, $replace, $text);
        }

        $this->_dbInstance->emit($text, PARSING_CHANNEL, 'redBright');

        self::addToFile($this->setDateToPath($this->_errorPath), $text);
    }

    function custom($text){
        self::addToFile($this->setDateToPath($this->_customPath), $text);
    }

    function proxyError($text, $search = '', $replace = ''){
        if($search){
            $text = str_replace($search, $replace, $text);
        }

        $this->_dbInstance->emit($text, PROXY_CHANNEL, 'redBright');

        self::addToFile($this->setDateToPath($this->_proxyPath), $text);
    }

    private function setDateToPath($filePath){
        return str_replace('{date}', $this->_timeInstance->getDate(0, 'Y-m-d'), $filePath);
    }

    static private function addToFile($filePath, $text, $isRaw = false, $type = 'a'){

        $pattern = quotemeta($text);

        if(! preg_match("#$pattern#", self::getLastItem($filePath))) {

            if(! $isRaw){
                $time = Time::getInstance();

                $debug = debug_backtrace(false, 2);
                $text = $time->getDate() . "   $text   " . basename($debug[1]['file']) . ':' . $debug[1]['line'];

                $text .= PHP_EOL;
            }

            if(file_exists($filePath)){
                if(! chmod($filePath, 0777)){
                    echo "Не могу выставить права 0777 для файла $filePath";
                    return;
                }
            }

            $fs = @fopen($filePath, $type);
            if (! flock($fs, LOCK_EX)) {
                echo str_replace('{name}', $filePath, CANT_LOCK_FILE_TO_WRITE);
                return;
            }

            fwrite($fs, $text);
            flock($fs, LOCK_UN);
            fclose($fs);
        }
    }
}