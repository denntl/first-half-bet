<?php

/**
 * Created by PhpStorm.
 * User: malina
 * Date: 25.12.2017
 * Time: 19:35
 */
class Strategy {

    private $_db;
    private $_time;
    private $_log;
    private $_lastMatches;
    private $_currentKey = 0;
    private $_mustDoBetLevel = 0;
    private $_loseCounter = 0;
    private $_checkManyWinsArr = [];

    public $mustDoBet = false;

    private static $_instance = null;

    protected function __clone() {}

    static public function getInstance() {
        if(is_null(self::$_instance)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct(){
        $this->_db = Db::getInstance();
        $this->_time = Time::getInstance();
        $this->_log = Log::getInstance();

//        $badChampionships = [];
//        foreach ($this->_db->getBadChampionships() as $badChampionship){
//            $badChampionships[] = $badChampionship['championship_id'];
//        }
//        $this->_log->setBadChampionships(implode(',', $badChampionships));

        $this->_db->updateLostMatches();

        $firstLoseMatchId = $this->_log->getFirstLoseMatch();

        if(! $firstLoseMatchId) {

            $notFinishedCount = $this->_db->getNotFinishedMatchesCount();

            if($notFinishedCount['not_finished'] == 0){

                $this->_lastMatches = $this->_db->getLastFinishMatches();

                $this->processMatches();

                $this->mustDoBet = (bool)$this->_mustDoBetLevel;

                if ($this->mustDoBet) {

                    $this->_log->bet(
                        BET_START,
                        'cyan',
                        ['{date}', '{id}'],
                        [
                            $this->_time->getDate(),
                            $this->_lastMatches[0]['id']
                        ]
                    );
                    $this->_log->setFirstLoseMatch($this->_lastMatches[0]['id']);
                }
            }

        } else {

            $profit = 0;
            $notFinished = 0;

            $lastBetMatches = $this->_db->getLastMatches($firstLoseMatchId);

            foreach ($lastBetMatches as $match) {
                if (! empty($match['bet'])) {
                    if (is_null($match['timestamp_end'])) {
                        $notFinished++;
                    }
                    $profit += $match['profit'];
                }
            }

            echo "Текущий профит $profit. ";

            if($this->checkDayToBet()){
                if($notFinished || $profit < 50){

                    $this->mustDoBet = true;
                } else {

                    $this->_log->bet(
                        BET_END,
                        'cyan',
                        ['{date}', '{profit}'],
                        [
                            $this->_time->getDate(),
                            $profit
                        ]
                    );
                    $this->_log->deleteFirstLoseMatch();
                }
            }
        }
    }

    private function processMatches(){
        foreach ($this->_lastMatches as $key => $match){
            $this->_currentKey = $key;
            $this->updateLoseCounter($match);
            if($this->updateMustDoBetLevel($key + 1)){
                $this->checkManyWins();
                return;
            }
        }
    }

    private function checkManyWins(){

        $from = $this->_currentKey + 1;
        $to = $from + 25;
        $this->processCheckManyWins($from, $to);

        /* Нет проигрышей за 25 матчей после тех игр, которые учавствуют в стратегии */
        if(array_sum($this->_checkManyWinsArr) == 0){
            $this->_mustDoBetLevel = 0;
            echo "Шаг 1";
            return;
        }

        $from = $to;
        $to = $from + 25;

        $this->processCheckManyWins($from, $to);

        /* Проигрышей за 50 матчей максимум 2 без учета игр, которые учавствуют в стратегии */
        if(array_sum($this->_checkManyWinsArr) < 3){
            $this->_mustDoBetLevel = 0;
            echo "Шаг 2";
            return;
        }

        $from = $to;
        $to = $from + 20;

        $this->processCheckManyWins($from, $to);

        /* Проигрышей за 70 матчей максимум 3 без учета игр, которые учавствуют в стратегии */
        if(array_sum($this->_checkManyWinsArr) < 4){
            $this->_mustDoBetLevel = 0;
            echo "Шаг 3";
            return;
        }

        $from = $to;
        $to = $from + 30;

        $this->processCheckManyWins($from, $to);

        /* Проигрышей за 100 матчей максимум 5 без учета игр, которые учавствуют в стратегии */
        if(array_sum($this->_checkManyWinsArr) < 6){
            $this->_mustDoBetLevel = 0;
            echo "Шаг 4";
            return;
        }
    }

    private function processCheckManyWins($from, $to){
        $loseCount = 0;

        for($i = $from; $i < $to; $i++){
            if(isset($this->_lastMatches[$i]) && $this->_lastMatches[$i]['profit'] == -100){
                $loseCount++;
            }
        }

        $this->_checkManyWinsArr[] = $loseCount;
    }

    private function checkDayToBet(){
        return date('l') != 'Saturday' && date('l') != 'Sunday' && date('l') != 'Thursday';
    }

    private function updateMustDoBetLevel($key){
        if($this->_lastMatches[0]['profit'] == -100 && $this->_mustDoBetLevel == 0 && $this->checkDayToBet()){
            if ($key < 6 && $this->_loseCounter > 2) {
                return $this->_mustDoBetLevel = 1;
            } elseif ($key < 8 && $this->_loseCounter > 3){
                return $this->_mustDoBetLevel = 2;
            } elseif ($key < 15 && $this->_loseCounter > 4){
                return $this->_mustDoBetLevel = 2;
            } elseif ($key < 18 && $this->_loseCounter > 5){
                return $this->_mustDoBetLevel = 2;
            } elseif ($key < 20 && $this->_loseCounter > 6){
                return $this->_mustDoBetLevel = 3;
            }
        }
        return false;
    }

    private function updateLoseCounter($match){
        if($match['profit'] == -100){
            $this->_loseCounter++;
        }
    }
}