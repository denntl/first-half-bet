<?php

/**
 * Created by PhpStorm.
 * User: malina
 * Date: 19.12.2017
 * Time: 13:27
 */
class Gmail {

    protected function __clone() {}

    private static $_instance = null;
    private $_clientSecretPath = __DIR__ . '/../config/client_secret.json';
    private $_oauthCallbackUrl = 'http://f459227f.ngrok.io/oauth2callback.php';
    private $_client;
    private $_service;
    private $_emptyToken = false;
    private $_gmailToken;
    private $_gmailTokenExample;

    static public function getInstance() {
        if(is_null(self::$_instance)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct(){

        $this->setClient();

        if ($this->_client->isAccessTokenExpired()) {
            $this->updateToken();
            $this->setClient();
        }

        $this->_service = new Google_Service_Gmail($this->_client);
    }

    private function setClient(){

        $this->_gmailToken = Log::getGmailToken();
        if(empty($this->_gmailToken)){
            $this->_gmailTokenExample = Log::getGmailTokenExample();
            if(empty($this->_gmailTokenExample)) {
                $this->_emptyToken = true;
                return;
            } else {
                if(Log::copyGmailToken()){
                    $this->_gmailToken = Log::getGmailToken();
                } else {
                    $this->_emptyToken = true;
                    return;
                }
            }
        }

        $client = new Google_Client();
        $client->setAuthConfig($this->_clientSecretPath);
        $client->setAccessType("offline");
        $client->setApprovalPrompt('force');
        $client->setIncludeGrantedScopes(true);
        $client->addScope(Google_Service_Gmail::GMAIL_SEND);
        $client->setRedirectUri($this->_oauthCallbackUrl);
        $client->setAccessToken(json_decode($this->_gmailToken, true));
        $this->_client = $client;
    }

    private function updateToken(){
        $refreshTokenSaved = $this->_client->getRefreshToken();
        $this->_client->fetchAccessTokenWithRefreshToken($refreshTokenSaved);
        $accessTokenUpdated = $this->_client->getAccessToken();
        $accessTokenUpdated['refresh_token'] = $refreshTokenSaved;
        $accessToken = $refreshTokenSaved;
        $this->_client->setAccessToken($accessToken);
        Log::setGmailToken(json_encode($accessTokenUpdated));
    }

    public function sendMail($subject, $text, $style = '', $loops = 3){

        $errors = [];
        $loopsArr = [];

        if(! $this->_emptyToken){

            for($i = 0; $i < $loops; $i++) {

                $loopsArr[] = 1;

                $messageString = base64_encode("To:denstels5@gmail.com\r\nSubject: =?utf-8?B?" . base64_encode($subject)
                    . "?=\r\nContent-Transfer-Encoding: base64\r\nContent-Type: text/html; charset=UTF-8\r\nDate:\r\nMessage-Id:\r\nFrom:\r\n"
                    . base64_encode("<div style=\"$style\"></div>$text"));

                $message = new Google_Service_Gmail_Message();
                $mime = rtrim(strtr($messageString, '+/', '-_'), '=');
                $message->setRaw($mime);

                try {
                    $this->_service->users_messages->send('me', $message);
                    return [
                        'status' => 'ok',
                        'loops' => array_sum($loopsArr),
                        'errors' => ''
                    ];
                } catch (Exception $e) {
                    $errors[] = $e;
                }
            }
        } else {
            $errors[] = 'Empty token file!';
        }

        return [
            'status' => 'error',
            'loops' => array_sum($loopsArr),
            'errors' => print_r($errors, true)
        ];
    }
}