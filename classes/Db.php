<?php

/**
 * Created by PhpStorm.
 * User: malina
 * Date: 20.08.2017
 * Time: 12:14
 */
class Db {

    private $dbh;
    private $redis;

    protected function __clone() {}

    private static $_instance = null;

    static public function getInstance($config = []) {
        if(is_null(self::$_instance)){
            self::$_instance = new self($config);
        }
        return self::$_instance;
    }

    private function __construct($config){

        $dsn = 'mysql:dbname=' . $config['mysql']['db'] . ';host=' . $config['mysql']['host'];
        try {
            $this->dbh = @new PDO($dsn, $config['mysql']['user'], $config['mysql']['password'], [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"]);
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage(); die();
        }

        $this->redis = new \Redis();
        $success = @$this->redis->connect($config['redis']['host'], $config['redis']['port']);
         if(! $success) {
            echo 'Подключение не удалось'; die();
        }
    }

    function emit($data, $channel, $color = ''){
        $data = ['data' => $data];
        if($color){
            $data['color'] = $color;
        }
        try {
            $this->redis->publish($channel, json_encode($data, JSON_UNESCAPED_UNICODE));
        } catch (RedisException $e){
            echo 'Emit error: ' . $e->getMessage(); die();
        }
    }

    function updateLostMatches(){
        $endTime = time() - 3600;
        $time = Time::getInstance();

        $stmt = $this->dbh->prepare('UPDATE `matches` SET `timestamp_end` = "' . $time->getDate() . '", `profit` = 0 WHERE 
            `timestamp_end` IS NULL AND `timestamp` < "' . $time->getDate($endTime) . '"');

        if(! $stmt->execute()){
            print_r($stmt->errorInfo()); die();
        }

        return $stmt->fetch();
    }

    function statGetMatches(){
        $endTime = time() - 30 * 86400;
        $time = Time::getInstance();

        $stmt = $this->dbh->prepare('SELECT 
            `timestamp`, timestamp_end, profit, first_name, second_name 
            FROM `matches` WHERE 
            `timestamp_end` IS NOT NULL 
            AND `rate` > 1.09 
            AND `online` < 60 
            AND `rate` < 1.32 
            AND championship_id NOT IN ('.Log::getBadChampionships().')
            AND DAYNAME(`timestamp`) != \'Saturday\'
            AND DAYNAME(`timestamp`) != \'Thursday\'
            AND DAYNAME(`timestamp`) != \'Sunday\'
            AND `score` != "1:0"
            AND `timestamp` > "' . $time->getDate($endTime) . '"');
        if(! $stmt->execute()){
            print_r($stmt->errorInfo()); die();
        }

        return $stmt->fetchAll();
    }

    function saveChampionship($name){
        $stmt = $this->dbh->prepare("SELECT `id` FROM championships WHERE `name` = :name");
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        try {
            $stmt->execute();
        } catch (PDOException $e) {
            throw $e;
        }

        if($stmt->rowCount() == 0){
            $stmt = $this->dbh->prepare("INSERT INTO championships (`name`) VALUES (:name)");
            $stmt->bindParam(':name', $name, PDO::PARAM_STR);
            try {
                $stmt->execute();
            } catch (PDOException $e) {
                throw $e;
            }
        }

        return $stmt->fetch()['id'];
    }

    function getBadChampionships(){
        $stmt = $this->dbh->prepare('
          SELECT m.championship_id
          FROM (
                SELECT SUM(`profit`) profit_sum, championship_id, COUNT(`id`) `matches_counter`
                FROM `matches`
                WHERE `rate` > 1.09
                AND `rate` < 1.32
                AND `online` < 60
                AND `profit` >= -100
                AND `profit` != 0
                AND `timestamp_end` IS NOT NULL
                AND DAYNAME(`timestamp`) != \'Saturday\'
                AND DAYNAME(`timestamp`) != \'Thursday\'
                AND DAYNAME(`timestamp`) != \'Sunday\'
                AND `score` != \'1:0\'
                GROUP BY `championship_id`
                ORDER BY profit_sum ASC
          ) m
          WHERE m.matches_counter > 9
          AND m.profit_sum <= -199'
        );
        try {
            $stmt->execute();
        } catch (PDOException $e) {
            throw $e;
        }

        return $stmt->fetchAll();
    }

    function getMatch($siteId, $teamNames){
        $stmt = $this->dbh->prepare('SELECT * FROM matches WHERE `site_id` = :siteId AND `first_name` = :firstName AND `second_name` = :secondName');
        $stmt->bindParam(':siteId', $siteId, PDO::PARAM_INT);
        $stmt->bindParam(':firstName', $teamNames[0], PDO::PARAM_STR);
        $stmt->bindParam(':secondName', $teamNames[1], PDO::PARAM_STR);
        try {
            $stmt->execute();
        } catch (PDOException $e) {
            throw $e;
        }

        return $stmt->fetch();
    }

    function getLastFinishMatches($limit = 150){
        $stmt = $this->dbh->prepare('SELECT * FROM matches 
            WHERE `timestamp_end` IS NOT NULL  
            AND `rate` > 1.09
            AND `rate` < 1.32
            AND `online` < 60
            AND championship_id NOT IN ('.Log::getBadChampionships().')
            AND DAYNAME(`timestamp`) != \'Saturday\'
            AND DAYNAME(`timestamp`) != \'Thursday\'
            AND DAYNAME(`timestamp`) != \'Sunday\'
            AND `score` != "1:0"
            ORDER BY `id` DESC LIMIT :limit');
        $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            throw $e;
        }

        return $stmt->fetchAll();
    }

    function getNotFinishedMatchesCount(){
        $stmt = $this->dbh->prepare('SELECT COUNT(`id`) not_finished FROM matches 
            WHERE `timestamp_end` IS NULL  
            AND `rate` > 1.09
            AND `rate` < 1.32
            AND `online` < 60
            AND championship_id NOT IN ('.Log::getBadChampionships().')
            AND DAYNAME(`timestamp`) != \'Saturday\'
            AND DAYNAME(`timestamp`) != \'Thursday\'
            AND DAYNAME(`timestamp`) != \'Sunday\'
            AND `score` != "1:0"');

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            throw $e;
        }

        return $stmt->fetch();
    }

    function getLastMatches($dbMatchId){
        $stmt = $this->dbh->prepare('SELECT * FROM matches 
            WHERE `id` > :id 
            AND `rate` > 1.09
            AND `rate` < 1.32
            AND `online` < 60
            AND championship_id NOT IN ('.Log::getBadChampionships().')
            AND DAYNAME(`timestamp`) != \'Saturday\'
            AND DAYNAME(`timestamp`) != \'Thursday\'
            AND DAYNAME(`timestamp`) != \'Sunday\'
            AND `score` != "1:0"
            ORDER BY `id` DESC');
        $stmt->bindParam(':id', $dbMatchId, PDO::PARAM_INT);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            throw $e;
        }

        return $stmt->fetchAll();
    }

    function saveMatch($siteId, $teamNames, $score, $time, $rate, $championshipId, $online, $bet = ''){

        $stmt = $this->dbh->prepare('INSERT INTO matches (`site_id`, `first_name`, `second_name`, `score`, `time`, `rate`, `championship_id`, `online`, `bet`) VALUES (:siteId, :firstName, :secondName, :score, :time, :rate, :championshipId, :online, :bet)');

        $stmt->bindParam(':siteId', $siteId, PDO::PARAM_INT);
        $stmt->bindParam(':firstName', $teamNames[0], PDO::PARAM_STR);
        $stmt->bindParam(':secondName', $teamNames[1], PDO::PARAM_STR);
        $stmt->bindParam(':score', $score, PDO::PARAM_STR);
        $stmt->bindParam(':time', $time, PDO::PARAM_STR);
        $stmt->bindParam(':rate', $rate, PDO::PARAM_STR);
        $stmt->bindParam(':championshipId', $championshipId, PDO::PARAM_INT);
        $stmt->bindParam(':online', $online, PDO::PARAM_INT);
        $stmt->bindParam(':bet', $bet, PDO::PARAM_STR);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            throw $e;
        }
    }

    function updateMatch($siteId, $teamNames, $score, $profit){
        $stmt = $this->dbh->prepare('UPDATE matches SET `result` = :result, `timestamp_end` = :timestampEnd, `profit` = :profit WHERE site_id = :siteId AND `first_name` = :firstName AND `second_name` = :secondName');

        $time = Time::getInstance();
        $timestampEnd = $time->getDate();

        $stmt->bindParam(':siteId', $siteId, PDO::PARAM_INT);
        $stmt->bindParam(':firstName', $teamNames[0], PDO::PARAM_STR);
        $stmt->bindParam(':secondName', $teamNames[1], PDO::PARAM_STR);
        $stmt->bindParam(':profit', $profit, PDO::PARAM_STR);
        $stmt->bindParam(':result', $score, PDO::PARAM_STR);
        $stmt->bindParam(':timestampEnd', $timestampEnd, PDO::PARAM_STR);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            throw $e;
        }
    }
}

// SELECT COUNT(`id`) as `Matches`,ROUND(SUM(`profit_tmr`),2) as `TMr`,  ROUND(SUM(`profit_tb`),2) as `TB`,  ROUND(SUM(`profit_tm`), 2) as `TM`,  ROUND(SUM(`profit_tbr`),2) as `TBr` FROM `matches` WHERE `timestamp_end` IS NOT NULL AND `TMr` > 1.09 AND `TMr` < 1.32
// SELECT `championship_id`, COUNT(`id`) as `Matches` FROM `matches` WHERE `profit_tmr` < 0 GROUP BY `championship_id` ORDER BY `Matches` DESC