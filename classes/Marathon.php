<?php

/**
 * Created by PhpStorm.
 * User: malina
 * Date: 05.08.2017
 * Time: 10:50
 */
class Marathon {

    private $_footballUrl = 'https://www.marathonbet.com/su/live/26418?pageAction=default';
    private $_matchUrl = 'https://www.marathonbet.com/su/live/';
    private $_ticketUrl = 'https://www.marathonbet.com/su/betslip/placebetinoneclick.htm';
    private $_betUrl = 'https://www.marathonbet.com/su/betslip/placeticket.htm';
    private $_cleanBetUrl = 'https://www.marathonbet.com/su/betslip/clear.htm';
    private $_liveFootballPage = '';
    
    public $onlineCount = 0;

    private $service;

    function __construct(){

        $this->service = Service::getInstance();

        $this->service->db->emit(
            str_replace(['{date}', '{proxy}'], [$this->service->time->getDate(), $this->service->proxy->currentProxy], PARSING_START),
            PARSING_CHANNEL,
            'cyan'
        );

        if($this->getFootballLive()){
            if($this->getOnlineCount()) {
                $this->getChampionships();
            } else {
                $this->service->log->error(NO_MATCHES);
            }
        }
    }

    function getOnlineCount(){
        preg_match_all("#data-event-treeId=#", $this->_liveFootballPage, $matchesCount);
        $this->onlineCount = count($matchesCount[0]);
        return $this->onlineCount;
    }

    function getFootballLive(){

        $this->_liveFootballPage = $this->service->curl->getUrl($this->_footballUrl, $this->service->proxy->currentProxyData);
        if(empty($this->_liveFootballPage)){
            $this->service->log->error(EMPTY_FOOTBALL_PAGE);
            return null;
        } else {
            if(preg_match("#has been blacklisted due to a high volume of requests|заблокирован в связи с большим количеством запросов#", $this->_liveFootballPage)){
                echo 'IP ' . $this->service->proxy->currentProxy . ' заблокирован.';
                $this->service->log->error(IP_IS_BLOCKED, '{ip}', $this->service->proxy->currentProxy);
                return null;
            } else {
                $this->_liveFootballPage = json_decode($this->_liveFootballPage, true);
                if (count($this->_liveFootballPage) == 0) {
                    $this->service->log->error(MATCHES_NOT_FOUND);
                    return null;
                } else {
                    foreach ($this->_liveFootballPage as $value) {
                        if ($value["selector"] == "#liveEventsContent") {
                            $this->_liveFootballPage = $value["content"];
                            $this->service->db->emit(LIVE_FOOTBALL_PAGE_OK, PARSING_CHANNEL, 'greenBright');
                            return true;
                        }
                    }
                    $this->service->log->error(JSON_PARSE_SELECTOR_FOOTBALL_PAGE_ERROR);
                    return null;
                }
            }
        }
    }

    function getChampionships(){
        $championshipExplode = explode('<h2 data-ellipsis="{}" class="category-label">', $this->_liveFootballPage);
        unset($championshipExplode[0]);
        if(count($championshipExplode) == 0){
            $this->service->log->error(GET_CHAMPIONSHIPS_ERROR);
            return;
        }
        $this->service->db->emit(CHAMPIONSHIPS_OK, PARSING_CHANNEL, 'greenBright');
        foreach ($championshipExplode as $championship){
            $this->parseChampionship($championship);
        }
    }

    function parseChampionship($championship){
        preg_match('#^(.+?)</h2>#', $championship, $championshipName);
        $championshipName = preg_replace ('/<[^>]*>/', '', $championshipName[1]);
        if(empty($championshipName)){
            $this->service->log->error(EMPTY_CHAMPIONSHIP_NAME);
            return;
        }
        $this->service->db->emit(
            str_replace('{name}', $championshipName,PROCESS_CHAMPIONSHIP),
            PARSING_CHANNEL,
            'greenBright'
        );
        $this->getMatches($championship, $championshipName);
    }

    function getMatches($championship, $championshipName){

            $matchesExplode = explode('data-expanded-event-treeId', $championship);

            unset($matchesExplode[0]);
            $matchesCount = count($matchesExplode);

            if($matchesCount == 0) {

                $matchesExplode = explode('data-event-treeId', $championship);

                unset($matchesExplode[0]);
                $matchesCount = count($matchesExplode);

                if($matchesCount == 0) {
                    $matchesExplode = explode('<tbody', $championship);

                    unset($matchesExplode[0]);
                    $matchesCount = count($matchesExplode);

                    if($matchesCount == 0){
                        $matchesExplode = explode('my-fav-selector-td', $championship);

                        unset($matchesExplode[0]);
                        $matchesCount = count($matchesExplode);

                        if($matchesCount == 0) {

                            $this->service->log->error(NO_MATCHES_IN_CHAMPIONSHIP,
                                '{name}',
                                $championshipName);
                            $this->service->log->custom("Новая ошибка: \r\n\r\n"
                                . $championship . "\r\n\r\n\r\n");
                            return;
                        }
                    }
                }
            }
        $this->service->db->emit(
            str_replace('{count}', $matchesCount,CHAMPIONSHIP_MATCHES_FIND),
            PARSING_CHANNEL,
            'greenBright'
        );
        foreach ($matchesExplode as $match){
            $this->parseMatch($match, $championshipName);
        }
    }

    function parseMatch($match, $championshipName){
        if(! $this->isMatchFinish($match)) {

            $matchSiteId = $this->getMatchSiteId($match, $championshipName);
            if (is_null($matchSiteId)) return;

            $time = $this->getMatchTime($match, $matchSiteId);
            if (is_null($time)) return;

            $teamNames = $this->getTeamNames($match, $matchSiteId);
            if (is_null($teamNames)) return;

            $score = $this->getScore($match, $matchSiteId);
            if (is_null($score)) return;

            $scoreGlobal = $this->getGlobalScore($score, $matchSiteId);
            if(is_null($scoreGlobal)) return;

            $scoreSum = array_sum($scoreGlobal);

            $dbMatch = $this->service->db->getMatch($matchSiteId, $teamNames);

            if ($time != 'Пер.'){

                $timeMinutes = $this->getMinutes($time, $matchSiteId);
                if (is_null($timeMinutes)) return;

                if($timeMinutes >= 26 && $timeMinutes <= 32){

                    if(! $dbMatch['id']){

                        $liveMatchPage = $this->service->curl->getUrl($this->_matchUrl . $matchSiteId, $this->service->proxy->currentProxyData);

                        if(empty($liveMatchPage)){
                            $this->service->log->error(EMPTY_RATES_PAGE, '{id}', $matchSiteId);
                            return null;
                        }

                        $rate = $this->parseRate($scoreSum, $liveMatchPage);

                        if($rate){

                            $mustDoBet = $this->service->strategy->mustDoBet && $rate > 1.09 && $rate < 1.32 && $this->onlineCount < 60 && $score != '1:0';

                            $currentChampionshipId = $this->service->db->saveChampionship($championshipName);

                            if($mustDoBet){
                                if(in_array($currentChampionshipId, explode(',', $this->service->log->getBadChampionships()))){
                                    $this->service->log->bet(
                                        SKIP_BET_BAD_CHAMPIONSHIP,
                                        'yellowBright',
                                        ['{names}', '{id}'],
                                        [implode(' - ', $teamNames), $currentChampionshipId]
                                    );
                                    $mustDoBet = false;
                                }
                            }

                            $this->service->db->saveMatch(
                                $matchSiteId,
                                $teamNames,
                                $score,
                                $time,
                                $rate,
                                $currentChampionshipId,
                                $this->onlineCount,
                                $mustDoBet ? 100 : ''
                            );

                            $this->service->db->emit(
                                str_replace('{names}', implode(' - ', $teamNames),MATCH_SAVED_TO_DB),
                                PARSING_CHANNEL,
                                'yellowBright'
                            );

                            if($mustDoBet){
                                $this->doBet($liveMatchPage, $scoreSum, $matchSiteId, $teamNames, $rate, $scoreGlobal);
                            }
                        }
                    } else {

                        $this->service->db->emit(
                            str_replace('{names}', implode(' - ', $teamNames),MATCH_ALREADY_SAVED_TO_DB),
                            PARSING_CHANNEL,
                            'yellow'
                        );
                    }
                }

            } else if(preg_match('#\(#', $score)){

                if($dbMatch['id'] && empty($dbMatch['result'])){

                    $scoreSumBefore = array_sum(explode(':', $dbMatch['score']));
                    $scoreDiff = $scoreSum - $scoreSumBefore;
                    $betSum = 100;

                    $profit = $this->getProfit($scoreDiff, $betSum, $dbMatch['rate']);

                    $msg = str_replace(
                        ['{names}', '{profit}'],
                        [implode(' - ', $teamNames), $profit],
                        MATCH_SAVED_FINISHED
                    );

                    $this->service->db->emit(
                        $msg,
                        PARSING_CHANNEL,
                        'whiteBright'
                    );

                    if(! empty($dbMatch['bet'])){
                        $this->service->log->bet($this->service->time->getDate() . " $msg", 'whiteBright');
                    }

                    $this->service->db->updateMatch($matchSiteId, $teamNames, $score, $profit);
                }
            }

            if($dbMatch['id'] && empty($dbMatch['result'])){

                $scoreSumBefore = array_sum(explode(':', $dbMatch['score']));
                $scoreDiff = $scoreSum - $scoreSumBefore;

                if($scoreDiff > 1){

                    $this->service->db->updateMatch($matchSiteId, $teamNames, $score, -100);

                    $msg = str_replace(
                        '{names}',
                        implode(' - ', $teamNames),
                        MATCH_SAVED_FINISHED_PREVIOUSLY
                    );

                    $this->service->db->emit(
                        $msg,
                        PARSING_CHANNEL,
                        'whiteBright'
                    );
                    if(! empty($dbMatch['bet']) && $dbMatch['rate'] > 1.09 && $dbMatch['rate'] < 1.32 && $dbMatch['online'] < 60 && $dbMatch['score'] != '1:0') {
                        $this->service->log->bet($this->service->time->getDate() . " $msg", 'redBright');
                    }
                }
            }
        }
    }

    function doBet($liveMatchPage, $scoreSum, $matchSiteId, $teamNames, $rate, $scoreGlobal){

        $start = microtime(true);

        $loginTime = $this->service->log->getLoginTime();

        if(empty($loginTime) || time() - $loginTime > 3600){
            $this->service->curl->getUrl('http://localhost/get-balance.php?run=1', [], '', false, 60);
        }

        $scoreSumReserved = $scoreSum + 1.5;

        preg_match('#data-selection-key="(\d+)@(Total_Goals_-_1st_Half(|\d+).Under_' . $scoreSumReserved . ')"#', $liveMatchPage, $reservedRate);

        if ($reservedRate[1] && $reservedRate[2]) {

            preg_match('#data-event-name="(.+?)"#', $liveMatchPage, $dataEventName);

            if($dataEventName[1]) {

                preg_match('#data-sel=\'({"sn":"Меньше ' . $scoreSumReserved . '","mn":"Тотал голов, 1-й тайм".+?})\'#', $liveMatchPage, $dataSel);

                $dataSel = json_decode($dataSel[1], true);

                if ($dataSel && count($dataSel)) {

                    $dataSel['prices'] = (object) $dataSel['prices'];
                    $dataSel['mid'] = $reservedRate[1];
                    $dataSel['u'] = $reservedRate[1] . ',' . $reservedRate[2];
                    $dataSel['en'] = $dataEventName[1];
                    $dataSel['l'] = true;

                    $dataSel = stripslashes(json_encode($dataSel, JSON_UNESCAPED_UNICODE));

                    $postData = 'ch=' . $dataSel . '&st=' . $this->service->config['bet'] . '&chd=true';

                    $ticket = json_decode($this->service->curl->getUrl($this->_ticketUrl, $this->service->proxy->currentProxyData, $postData, true), true);

                    if ($ticket['ticket_id']) {

                        $postData = 't=' . $ticket['ticket_id'] . '&oneClick=true';

                        $success = [];
                        $loops = [];

                        sleep(6);

                        for ($i = 0; $i < 5; $i++) {
                            $res = json_decode($this->service->curl->getUrl($this->_betUrl, $this->service->proxy->currentProxyData, $postData, true), true);

                            $loops[] = 1;

                            if ($res["result"] == "OK") {
                                $success[] = 1;
                                break;
                            }

                            usleep(500000);
                        }

                        $loops = array_sum($loops);

                        $this->service->curl->getUrl($this->_cleanBetUrl, $this->service->proxy->currentProxyData, true, true);

                        if(array_sum($success)){

                            $time = round(microtime(true) - $start, 2);

                            $this->service->log->bet(
                                SUCCESS_PLACE_BET,
                                'yellowBright',
                                ['{date}', '{loops}', '{time}', '{name}', '{rate}', '{score}'],
                                [
                                    $this->service->time->getDate(),
                                    $loops,
                                    $time,
                                    implode(' - ', $teamNames),
                                    $rate,
                                    implode(':', $scoreGlobal),
                                ]
                            );

                            return true;

                        } else {

                            $this->service->log->bet(
                                WRONG_PLACE_BET,
                                'redBright',
                                ['{date}', '{id}', '{loops}'],
                                [
                                    $this->service->time->getDate(),
                                    $matchSiteId,
                                    $loops
                                ]
                            );
                        }

                    } else {

                        $this->service->log->bet(
                            CANT_CREATE_TICKET,
                            'redBright',
                            ['{date}', '{id}', '{data}'],
                            [
                                $this->service->time->getDate(),
                                $matchSiteId,
                                $postData
                            ]
                        );
                    }
                } else {

                    $this->service->log->bet(
                        CANT_FIND_DATA_SEL,
                        'redBright',
                        ['{date}', '{id}'],
                        [
                            $this->service->time->getDate(),
                            $matchSiteId
                        ]
                    );
                }
            } else {

                $this->service->log->bet(
                    CANT_FIND_EVENT_NAME,
                    'redBright',
                    ['{date}', '{id}'],
                    [
                        $this->service->time->getDate(),
                        $matchSiteId
                    ]
                );
            }
        } else {

            $this->service->log->bet(
                CANT_FIND_RESERVED_RATE,
                'redBright',
                ['{date}', '{id}'],
                [
                    $this->service->time->getDate(),
                    $matchSiteId
                ]
            );
        }

        return false;
    }

    function getProfit($scoreDiff, $betSum, $rate){
        if($scoreDiff > 1) {
            return -$betSum;
        }

        return $rate  * $betSum - $betSum;
    }

    function parseRate($scoreSum, $liveMatchPage){
        $scoreSumReserved = $scoreSum + 1;

        preg_match("#data-selection-key=\"\d+@Total_Goals_-_1st_Half(|\d+).Under_$scoreSumReserved.5\"\n>(\d+.\d+)</span>#", $liveMatchPage, $tmr);

        return isset($tmr[2]) ? $tmr[2] : null;
    }

    function getMinutes($time, $matchSiteId){
        preg_match('#^(\d+):#', $time, $minutes);
        if (strlen($minutes[1]) == 0) {
            $this->service->log->error(MINUTES_PARSE_ERROR, '{id}', $matchSiteId);
            return null;
        }
        return $minutes[1];
    }

    function getGlobalScore($score, $matchSiteId){
        preg_match('#^(\d+:\d+)#', $score, $globalScore);
        if(empty($globalScore[1])){
            $this->service->log->error(GLOBAL_SCORE_PARSE_ERROR, '{id}', $matchSiteId);
            return null;
        }
        $scoreGlobalExplode = explode(':', $globalScore[1]);
        if(strlen($scoreGlobalExplode[0]) == 0 || strlen($scoreGlobalExplode[1]) == 0){
            $this->service->log->error(GLOBAL_SCORE_ARRAY_PARSE_ERROR, '{id}', $matchSiteId);
            return null;
        }
        return $scoreGlobalExplode;
    }

    function isMatchFinish($match){
        return preg_match('#cl-left grey#', $match);
    }

    function getScore($match, $matchSiteId){
        preg_match('#<div  class="cl-left red">(.+?)<#', $match, $matchScoreBlock);
        if(empty($matchScoreBlock[1])){
            $this->service->log->error(SCORE_BLOCK_PARSE_ERROR, '{id}', $matchSiteId);
            return null;
        }
        $score = trim($matchScoreBlock[1]);
        if (empty($score)) {
            $this->service->log->error(SCORE_PARSE_ERROR, '{id}', $matchSiteId);
            return null;
        }
        return $score;
    }

    function getMatchTime($match, $matchSiteId){
        preg_match('#<span class="time-description">.+?</div>#', $match, $matchTimeBlock);
        if(empty($matchTimeBlock[0])){
            $this->service->log->error(TIME_BLOCK_PARSE_ERROR, '{id}', $matchSiteId);
            return null;
        }
        preg_match('#\d+\:\d+|Пер.#', $matchTimeBlock[0], $matchTime);
        if (empty($matchTime[0])) {
            $this->service->log->error(TIME_PARSE_ERROR, '{id}', $matchSiteId);
            return null;
        }
        return $matchTime[0];
    }

    function getMatchSiteId($match, $championshipName){
        preg_match('#data-favorites-selector="(\d+)"#', $match, $matchSiteId); // data-event-treeId
        $matchSiteId = (int) $matchSiteId[1];
        if($matchSiteId == 0){
            $this->service->log->error(WRONG_MATCH_ID, '{name}', $championshipName);
            return null;
        }
        return $matchSiteId;
    }

    function getTeamNames($match, $matchSiteId){
        preg_match('#data-event-name="(.+?)"#', $match, $teamNames);
        $teamNamesExplode = explode(' - ', $teamNames[1]);
        if(strlen($teamNamesExplode[0]) == 0 || strlen($teamNamesExplode[1]) == 0){
            $this->service->log->error(WRONG_COMMAND_NAMES, '{id}', $matchSiteId);
            return;
        }
        return $teamNamesExplode;
    }
}