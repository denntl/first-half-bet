<?php

/**
 * Created by PhpStorm.
 * User: malina
 * Date: 19.12.2017
 * Time: 12:45
 */
class Service
{
    private static $_instance = null;

    protected function __clone() {}

    public $db;
    public $log;
    public $gmail;
    public $curl;
    public $time;
    public $proxy;
    public $config;
    public $strategy;

    private function __construct() {

        $this->config = (array) require_once(__DIR__ . '/../config/config.php');

        $this->time = Time::getInstance();
        $this->db = Db::getInstance($this->config);
        $this->log = Log::getInstance();
        $this->gmail = Gmail::getInstance();
        $this->curl = Curl::getInstance();
        $this->proxy = Proxy::getInstance();
        $this->strategy = Strategy::getInstance();
    }

    static public function getInstance() {
        if(is_null(self::$_instance)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}