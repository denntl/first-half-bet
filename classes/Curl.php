<?php

/**
 * Created by PhpStorm.
 * User: malina
 * Date: 05.08.2017
 * Time: 11:54
 */
class Curl {

    private $proxy;

    protected function __clone() {}

    private static $_instance = null;

    static public function getInstance() {
        if(is_null(self::$_instance)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct(){
        $this->proxy = Proxy::getInstance();
    }

    function getUrl($url, $currentProxyData = [], $post = "", $cookies = false, $timeout = 25, $fresh = false){

        $startTime = microtime(true);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');
        curl_setopt($ch, CURLOPT_REFERER, "https://www.marathonbet.com/ru/");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,$timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        if($post) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        if($fresh){
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1);
        }
        if($currentProxyData){
            curl_setopt($ch, CURLOPT_PROXY, $currentProxyData[0]);
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $currentProxyData[1]);
        }
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if($cookies) {
            curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__ . '/../config/cookies.txt');
            curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__ . '/../config/cookies.txt');
        }
        $body = curl_exec($ch);
        curl_close($ch);

        $this->proxy->logParsingTime($startTime, $url, $currentProxyData[0]);

        return $body;
    }
}