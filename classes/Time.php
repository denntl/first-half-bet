<?php

/**
 * Created by PhpStorm.
 * User: malina
 * Date: 21.12.2017
 * Time: 14:23
 */
class Time {

    protected function __clone() {}

    private static $_instance = null;

    static public function getInstance() {
        if(is_null(self::$_instance)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct(){}

    public function getDate($time = 0, $format = 'Y-m-d H:i:s'){
        return date($format, $time ? $time : time());
    }
}