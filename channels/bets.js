/**
 * Created by malina on 20.08.2017.
 */

var config = {};
var host = '';
var port = 0;
var portRegExp = new RegExp('-port');
var hostRegExp = new RegExp('-host');

process.argv.forEach(function (val, index, array) {
    if(portRegExp.test(val)){
        port = val.replace('-port=', '');
    } else if(hostRegExp.test(val)){
        host = val.replace('-host=', '');
    }
});

if(host && port){
    config = {host: host, port: port};
}

var redis = require("redis"), client = redis.createClient(config);

var clc = require('cli-color');

client.on("error", function (err) {
    console.log("Error " + err);
});

client.on("message", function (channel, message) {
    if(typeof message !== 'object'){
        message = JSON.parse(message);
    }
    if(typeof message.data === 'undefined'){
        console.log('Нет message.data: ' + message);
        return;
    }
    if(message.color){
        var colors = message.color.split(".");
        var temp = clc;
        for(var key in colors){
            temp = temp[colors[key]];
        }
        console.log(temp(message.data));
    } else {
        console.log(message.data);
    }
});

client.subscribe("bets");