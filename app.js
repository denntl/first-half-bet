/**
 * Created by malina on 20.08.2017.
 */

var needle = require('needle');
var isWin = /^win/.test(process.platform);
var globalHost = isWin ? 'http://first-half-bet/' : 'http://localhost/';

/* Run other scripts */
var ngrokCommand = '';
var parsingCommand = '';
var proxyCommand = '';
var balanceCommand = '';
var betsCommand = '';
var emailCommand = '';

/* Redis config for windows */
var redisHost = '0.tcp.ngrok.io';
var redisPort = 15019;

if(isWin){
    ngrokCommand = 'start cmd.exe /K C:\\Users\\malina\\Desktop\\ngrok http 80';
    parsingCommand = 'start cmd.exe /K node channels/parsing.js -host=' + redisHost + ' -port=' + redisPort;
    proxyCommand = 'start cmd.exe /K node channels/proxy.js -host=' + redisHost + ' -port=' + redisPort;
    balanceCommand = 'start cmd.exe /K node channels/balance.js -host=' + redisHost + ' -port=' + redisPort;
    betsCommand = 'start cmd.exe /K node channels/bets.js -host=' + redisHost + ' -port=' + redisPort;
    emailCommand = 'start cmd.exe /K node channels/email.js -host=' + redisHost + ' -port=' + redisPort;
} else {
    ngrokCommand = 'gnome-terminal -x sh -c "/home/denntl/Programs/ngrok http 80; $SHELL; exec bash"';
    parsingCommand = 'gnome-terminal -x sh -c "node channels/parsing.js; $SHELL; exec bash"';
    proxyCommand = 'gnome-terminal -x sh -c "node channels/proxy.js; $SHELL; exec bash"';
    balanceCommand = 'gnome-terminal -x sh -c "node channels/balance.js; $SHELL; exec bash"';
    betsCommand = 'gnome-terminal -x sh -c "node channels/bets.js; $SHELL; exec bash"';
    emailCommand = 'gnome-terminal -x sh -c "node channels/email.js; $SHELL; exec bash"';
}

var child_process = require('child_process');
child_process.exec(ngrokCommand);
child_process.exec(parsingCommand);
child_process.exec(proxyCommand);
child_process.exec(balanceCommand);
child_process.exec(betsCommand);
child_process.exec(emailCommand);

/* App start */
function appStart (host, params) {
    params = typeof params !== 'undefined' && params ? params : '';
    needle.get(host + params, {open_timeout: 30000}, function (error, response) {
        if(typeof response !== 'undefined' && response.body){
            console.log(response.body);
        }
        if(typeof error !== 'undefined' && error){
            console.log(error);
        }
        setTimeout(function () {
            appStart(host);
        }, 10000);
    });
}

/* Ngrok config */
function ngrokConfig (host) {
    needle.get(host, {open_timeout: 30000}, function (error, response) {
        if(typeof response !== 'undefined' && response.body){
            console.log(response.body);
        }
        if(typeof error !== 'undefined' && error){
            console.log(error);
        }
    });
}

if(! isWin) {
    setTimeout(function () {
        appStart(globalHost + 'run.php', '?send-start-email=1');
        ngrokConfig(globalHost + 'ngrok-host.php?run=1');
    }, 5000);
} else {
    console.log('Режим просмотра!');
}