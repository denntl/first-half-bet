<?php

$start = microtime(true);

require('./config/global-vars.php');

require('./google-api-php-client-2.2.1/vendor/autoload.php');
require('./classes/Gmail.php');
require('./classes/Time.php');
require('./classes/Db.php');
require('./classes/Log.php');
require('./classes/Curl.php');
require('./classes/Marathon.php');
require('./classes/Proxy.php');
require('./classes/Strategy.php');
require('./classes/Service.php');

$service = Service::getInstance();

function canDoBet($loseArr){

    $loseCount = 0;
    foreach ($loseArr as $item){
        if($item) $loseCount++;
    }

    return $loseCount > 2;
}

$profit = 0;
$data = [0];
$points = [];
$nullArray = [];
$betStarted = false;
$betCount = 0;
$profits = [];
$betDataGlobal = $betData = ['count' => 0, 'profit' => 0, 'time_start' => '', 'time_end' => ''];
$needleProfit = 10;
$summaryProfit = 0;
$maxBetsNeedle = 0;
$blockedKey = -1;
$matches = $service->db->statGetMatches();
$matchesCount = count($matches);
$loseArrGlobal = $loseArr = [
    0 => false,
    1 => false,
    2 => false,
    3 => false,
    4 => false,
    5 => false,
];

foreach($matches as $key => $match){

    $loseArr = [
        0 => isset($matches[$key - 5]) && $blockedKey < ($key - 5) && $matches[$key - 5]['profit'] == -100,
        1 => isset($matches[$key - 4]) && $blockedKey < ($key - 4) && $matches[$key - 4]['profit'] == -100,
        2 => isset($matches[$key - 3]) && $blockedKey < ($key - 3) && $matches[$key - 3]['profit'] == -100,
        3 => isset($matches[$key - 2]) && $blockedKey < ($key - 2) && $matches[$key - 2]['profit'] == -100,
        4 => isset($matches[$key - 1]) && $blockedKey < ($key - 1) && $matches[$key - 1]['profit'] == -100,
        5 => $matches[$key]['profit'] == -100 ? true : false,
    ];

    if($betStarted){
        if($betData['profit'] >= $needleProfit || $matchesCount == $key + 1){

            $betData['time_end'] = $match['timestamp_end'];
            $summaryProfit += $betData['profit'];
            $maxBetsNeedle = $maxBetsNeedle < $betData['count'] ? $betData['count'] : $maxBetsNeedle;

            array_push($profits, $betData);

            $betStarted = false;
            $betData = $betDataGlobal;
            $blockedKey = $key;

        } else {
            $betData['profit'] += $match['profit'];
            $betData['count']++;
        }

    } else if(canDoBet($loseArr)){

        $betCount++;
        $betStarted = true;
        $betData['time_start'] = $match['timestamp'];
    }

    $profit += $match['profit'];
    $data[] = $profit;
}

$time = round(microtime(true) - $start, 2);

echo '<div style="margin-left: 16300px; width: 640px">Bets period count: ' . $betCount .', Summary profit: '. $summaryProfit . ', Max bets needle: ' . $maxBetsNeedle . ', Ставим: ' . $service->strategy->mustDoBet . ', Время: ' . $time . 'с.</div>   ';

$series = json_encode([
    (object) [
        'name' => 'Installation',
        'data' => $data
    ]
]);
?>

<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>

<div id="container" style="min-width: 17000px; height: 600px; margin: 0 auto"></div>

<script>
    Highcharts.chart('container', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Monthly Average Temperature'
        },
        subtitle: {
            text: 'Source: WorldClimate.com'
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: <?= $series ?>
    });
</script>